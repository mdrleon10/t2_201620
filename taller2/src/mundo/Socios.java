package mundo;

import java.util.Date;

public class Socios extends Trabajo
{

	public enum SociosEvento
	{
		EMPRESA1,
		EMPRESA2,
		EMPRESA3,
		OTROS
	}
	
	protected SociosEvento socioEmpresa;
	
	public Socios(Date pFecha, String pLugar, boolean pObligatorio,
			boolean pFormal, TipoEventoTrabajo pTipo, SociosEvento pSocioEmpresa) 
	{
		super(pFecha, pLugar, pObligatorio, pFormal, pTipo);
		socioEmpresa = pSocioEmpresa;
	}
	
	public String conv (Boolean b)
	{
		if (!b) return "No";
		else
			return "Si";
	}
	
	public String toString()
	{
	  return "Evento con FAMILIA: \n"
	  		+"FECHA: " + this.fecha
	  		+"\nLUGAR: " + this.lugar
	  		+"\nTIPO EVENTO: " + this.tipo
	  		+"\nOBLIGATORIO: " + conv(this.obligatorio)
	  		+"\nFORMAL: "+ conv(this.formal);
	}
}

